package org.jan.uniapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.jan.uniapp.domain.Student;
import org.jan.uniapp.repository.DatabaseHelper;
import org.jan.uniapp.utils.CustomizedExceptionHandler;

/**
 * Created by Gianluca on 06/04/2017.
 */

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();
    ///THIS SHOULD BE ONLy for debug TODO remove or to do it better
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(SplashActivity.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(SplashActivity.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Toast.makeText(SplashActivity.this,
                    "Write External Storage permission allows us to do store crash info." +
                            " Please allow this permission in App Settings.",
                    Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 42);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 42) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Thread.setDefaultUncaughtExceptionHandler(new CustomizedExceptionHandler(Environment.getExternalStorageDirectory().getAbsolutePath()));
            }
            return;
        }
    }


    ///////////////DBG
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /////DBG TODO: REMOVE or do it better
        // Sets the default uncaught exception handler. This handler is invoked
        // in case any Thread dies due to an unhandled exception.
        if (!checkPermission()) {
            requestPermission();
        } else {
            Thread.setDefaultUncaughtExceptionHandler(
                    new CustomizedExceptionHandler(
                            Environment.getExternalStorageDirectory().getAbsolutePath()
                    )
            );
        }
        //DBG
        ////CHECK IF LOGGED
        Log.v(TAG, "check if a user exist in database");
        DatabaseHelper dbh = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        Student stu = null;
        try {
            stu = dbh.getStudent();
        } catch (Exception e) {
            Log.e(getClass().getName(), "Getting student from db", e);
        }
        Intent intent = null;
        if (stu != null) {
            Log.v(TAG, "Student already logged. Start main");
            intent = new Intent(getApplicationContext(), MainActivity.class);
        } else {
            Log.v(TAG, "Student not logged. Start login");
            intent = new Intent(getApplicationContext(), LoginActivity.class);
        }
        ///IF FIRST RUN
        SharedPreferences sp = android.preference.PreferenceManager.getDefaultSharedPreferences(this);
        if (sp.getBoolean("FIRST_RUN", true)) {
            Log.v(TAG, "First run, load default preference");
            PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
            sp.edit()
                    .putBoolean("FIRST_RUN", false)
                    .apply();
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }
}
