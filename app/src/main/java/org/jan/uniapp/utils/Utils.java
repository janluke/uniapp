package org.jan.uniapp.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

/**
 * Created by jan on 30/03/17.
 */

public class Utils {
    public static Bitmap getCircularBitmapFromBlob(byte[] blob) {
        Bitmap output;
        Bitmap input = BitmapFactory.decodeByteArray(blob, 0, blob.length);

        if (input.getWidth() > input.getHeight()) {
            output = Bitmap.createBitmap(input.getHeight(), input.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(input.getWidth(), input.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, input.getWidth(), input.getHeight());

        float r = 0;

        if (input.getWidth() > input.getHeight()) {
            r = input.getHeight() / 2;
        } else {
            r = input.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(input, rect, rect, paint);
        return output;
    }

    public static String toFirstCharUpper(String string) {
        if (string == null) return null;
        String tmp = string.substring(0, 1).toUpperCase() +
                string.substring(1).toLowerCase();
        return tmp;
    }

}
