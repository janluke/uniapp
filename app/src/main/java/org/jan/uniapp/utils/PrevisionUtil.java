package org.jan.uniapp.utils;

import org.jan.uniapp.domain.Course;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Gianluca on 11/04/2017.
 */

public class PrevisionUtil {
    public final static int
            CALCULATE_VOTES_THROUGH_AVERANGE = 1,
            CALCULATE_AVERANGE_THROUGH_VOTES = 0;

    private int value30L = 31;
    private int mode = CALCULATE_VOTES_THROUGH_AVERANGE;
    private HashMap<String, Integer> votes, userSettedVotes;
    private List<Course> courses;
    private double averange, actualAverange;
    private double maxAverange, minAverange;

    public class PrevisionException extends Exception {
        public PrevisionException(String message) {
            super(message);
        }
    }

    public PrevisionUtil(List<Course> allSignificantCourses) {
        this.courses = allSignificantCourses;//solo esami con cfu>0
        reset();

    }

    public final void reset() {
        this.votes = new HashMap<String, Integer>();
        this.userSettedVotes = new HashMap<>();
        this.votes = new HashMap<>();
        calculateValues();
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public void setDegreeVote(double degreeVote) {
        this.averange = degreeVote * 3 / 11;
        updateValues();
    }

    public void setVote(Course course, Integer value) {
        userSettedVotes.put(course.getCode(), value);
        updateValues();
    }

    public void setAverange(double averange) {
        this.averange = averange;
        updateValues();
    }


    private void updateValues() {
        double allCfuSum = 0,
                cfuSum = 0,
                weightVotesSum = 0;
        switch (mode) {
            case CALCULATE_VOTES_THROUGH_AVERANGE:
                for (Course c : courses) {
                    if (c.getFinalResult() == 0
                            & !userSettedVotes.containsKey(c.getCode())
                            ) {
                        cfuSum += c.getCfu();
                        allCfuSum += c.getCfu();
                    } else if (c.getFinalResult() > 1) {
                        allCfuSum += c.getCfu();
                        weightVotesSum += c.getCfu() * (c.getFinalResult() > 30 ? value30L : c.getFinalResult());
                    } else if (userSettedVotes.containsKey(c.getCode())) {
                        allCfuSum += c.getCfu();
                        weightVotesSum += userSettedVotes.get(c.getCode()) * c.getCfu();
                    }
                }
                double votoMin = ((allCfuSum * averange) - weightVotesSum) / cfuSum;
                for (Course c : courses) {
                    if (c.getFinalResult() == 0) {
                        if (userSettedVotes.containsKey(c.getCode())) {
                            votes.put(c.getCode(), userSettedVotes.get(c.getCode()));
                        } else {
                            votes.put(c.getCode(), (int) Math.round(votoMin));
                        }
                    }
                }
                break;
            case CALCULATE_AVERANGE_THROUGH_VOTES:
                for (Course c : courses) {
                    if (c.getFinalResult() > 1) {
                        allCfuSum += c.getCfu();
                        weightVotesSum += c.getCfu() * (c.getFinalResult() > 30 ? value30L : c.getFinalResult());
                    } else if (userSettedVotes.containsKey(c.getCode())) {
                        votes.put(c.getCode(), userSettedVotes.get(c.getCode()));
                        allCfuSum += c.getCfu();
                        weightVotesSum += userSettedVotes.get(c.getCode()) * c.getCfu();
                    }
                    this.averange = weightVotesSum / allCfuSum;
                }
        }
    }

    private void calculateValues() {
        double cfuSum = 0, voteSum = 0;
        double voteSumMax = 0, voteSumMin = 0, cfuSum_ = 0;
        for (Course c : courses) {
            if (c.getFinalResult() > 1) {
                cfuSum += c.getCfu();
                voteSum += c.getCfu() * c.getFinalResult();
            } else if (c.getFinalResult() == 0) {
                cfuSum_ += c.getCfu();
                voteSumMax += 30 * c.getCfu();
                voteSumMin += 18 * c.getCfu();
            }
        }
        cfuSum_ += cfuSum;
        this.actualAverange = voteSum / cfuSum;
        this.maxAverange = (voteSumMax + voteSum) / cfuSum_;
        this.minAverange = (voteSumMin + voteSum) / cfuSum_;
        setAverange(actualAverange);
    }

    public double getAverange() {
        return averange;
    }

    public double getDegreeVote() {
        return averange * 11 / 3;
    }

    public Integer getVote(Course course) {
        return votes.get(course.getCode());
    }

    public double getMaxAverange() {
        return maxAverange;
    }

    public double getMinAverange() {
        return minAverange;
    }

    public double getMaxDegreeVote() {
        return maxAverange * 11 / 3;
    }

    public double getMinDegreeVote() {
        return minAverange * 11 / 3;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public boolean remove(Course course) {
        if (userSettedVotes.containsKey(course.getCode())) {
            userSettedVotes.remove(course.getCode());
        }
        return courses.remove(course);
    }

    public void setValue30L(int value30L) {
        this.value30L = value30L;
    }
}
