package org.jan.uniapp.utils;

import org.jan.uniapp.domain.Course;

import java.util.List;

/**
 * Created by Gianluca on 26/04/2017.
 */

public class SummaryUtil {
    private double gainedCfu, totalCfu;
    private double averange;
    private List<Course> courses;

    public SummaryUtil(List<Course> courses) {
        this.courses = courses;
        calculateAll();
    }

    public int getGainedCfu() {
        return (int) gainedCfu;
    }

    public int getTotalCfu() {
        return (int) totalCfu;
    }

    public double getAverange() {
        return averange;
    }

    public double getDegreeVote() {
        return averange * 11 / 3;
    }

    private void calculateAll() {
        this.totalCfu = 0;
        double sum = 0, cfu_count = 0;
        this.gainedCfu = 0;

        for (Course c : courses) {
            totalCfu += c.getCfu();
            if (c.getFinalResult() != 0) {
                int fr = c.getFinalResult();
                gainedCfu += c.getCfu();
                if (fr > 1) {
                    sum += fr * c.getCfu();
                    cfu_count += c.getCfu();
                }
            }
        }

        this.averange = sum / cfu_count;

    }
}
