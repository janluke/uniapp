package org.jan.uniapp.utils;

import android.util.Log;

import org.apache.log4j.WriterAppender;
import org.jan.e3.utils.LoggerHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jan on 03/04/17.
 */
public class CustomizedExceptionHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler defaultUEH;
    private String localPath;

    private ByteArrayOutputStream logStream;

    public CustomizedExceptionHandler(String localPath) {
        this.localPath = localPath;
        //Getting the the default exception handler
        //that's executed when uncaught exception terminates a thread
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        logStream = new ByteArrayOutputStream();
        LoggerHelper.addAppender(new WriterAppender(LoggerHelper.getLayout(), logStream));

    }

    public void uncaughtException(Thread t, Throwable e) {
        try {
            logStream.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        //Write a printable representation of this Throwable
        //The StringWriter gives the lock used to synchronize access to this writer.
        final Writer stringBuffSync = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringBuffSync);
        e.printStackTrace(printWriter);
        String stacktrace = stringBuffSync.toString();
        printWriter.close();

        if (localPath != null) {
            try {
                writeToFile(
                        "[E3Wrapper]\n" + logStream.toString("utf-8")
                                + "[CAUSE]\n" + stacktrace);
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        }

//Used only to prevent from any code getting executed.
        // Not needed in this example
        defaultUEH.uncaughtException(t, e);
    }

    private void writeToFile(String currentStacktrace) {
        try {
            File dir = new File(localPath +
                    "/Crash_Reports");
            if (!dir.exists()) {
                dir.mkdirs();
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            Date date = new Date();
            String filename = dateFormat.format(date) + ".uniapp.txt";

            // Write the file into the folder
            File reportFile = new File(dir.getAbsolutePath() + File.separator + filename);
            FileWriter fileWriter = new FileWriter(reportFile);
            fileWriter.append(currentStacktrace);
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            Log.e("ExceptionHandler", e.getMessage());
        }
    }

}