package org.jan.uniapp.utils;

import org.jan.uniapp.domain.Course;
import org.jan.uniapp.domain.Exam;
import org.jan.uniapp.domain.Payment;
import org.jan.uniapp.domain.Result;
import org.jan.uniapp.domain.Student;

/**
 * Created by Gianluca on 07/06/2017.
 */

public class DomainFactory {

    public static Exam getInstance(org.jan.e3.domain.Exam exam) {
        Exam out = new Exam();
        out.setBooked(exam.getBooked());
        out.setBookedStudents(exam.getBookedStudents());
        out.setOpenBookingDate(exam.getOpenBookingDate());
        out.setCloseBookingData(exam.getCloseBookingData());
        out.setCourseName(Utils.toFirstCharUpper(exam.getName()));
        out.setDate(exam.getDate());
        out.setDescription(Utils.toFirstCharUpper(exam.getDescription()));
        out.setBookedStudents(exam.getBookedStudents());
        out.setId(Long.valueOf(exam.hashCode()));
        out.setLocation(exam.getLocation());
        out.setTeacher(exam.getTeacher());
        out.setCourseCode(exam.getCourseCode());
        org.jan.e3.domain.Result res = exam.getResult();
        if (res != null) {
            out.setBooked(true);
            Result r = getInstance(res);
            // r.setDate(exam.getDate());
            out.setResult(r);
        }
        return out;
    }

    public static Result getInstance(org.jan.e3.domain.Result result) {
        Result out = new Result();
        switch (result.getStatus()) {
            case ACCEPTED:
                out.setStatus(Result.ResultStatus.ACCEPTED);
                break;
            case PENDING:
                out.setStatus(Result.ResultStatus.PENDING);
                break;
            case REFUSED:
                out.setStatus(Result.ResultStatus.REFUSED);
                break;
        }
        out.setValue(result.getValue());
        return out;
    }

    public static Course getInstance(org.jan.e3.domain.Course course) {
        Course out = new Course();
        out.setCode(course.getCode());
        out.setName(Utils.toFirstCharUpper(course.getName()));
        out.setCfu(course.getCfu());
        out.setYear(course.getYear());
        out.setFinalResult(course.getFinalResult());
        out.setFinalResultDate(course.getResultDate());
        /*if(course.getFinalResult()!=0) {
            Result r = new Result();
            r.setStatus(Result.ResultStatus.ACCEPTED);
            r.setValue(course.getFinalResult());
            r.setDate(course.getResultDate());
            out.setResult(r);
        }*/
        out.setId(Long.valueOf(course.hashCode()));
        return out;
    }

    public static Student getInstance(org.jan.e3.domain.Student student) {
        Student out = new Student();
        out.setSurname(Utils.toFirstCharUpper(student.getSurname()));
        out.setSubscriptionDate(student.getSubscriptionDate());
        out.setName(Utils.toFirstCharUpper(student.getName()));
        out.setStudyRoute(student.getStudyRoute());
        out.setStudyCourse(student.getStudyCourse());
        out.setMatricola(student.getMatricola());
        out.setYear(student.getYear());
        out.setPhotoBlob(student.getPhotoBlob());
        return out;
    }

    public static Payment getInstance(org.jan.e3.domain.Payment payment) {
        Payment out = new Payment();
        out.setCode(payment.getCode());
        out.setDescription(payment.getDescription());
        out.setDueDate(payment.getDueDate());
        out.setValue(payment.getValue());
        switch (payment.getStatus()) {
            case CONFIRMED:
                out.setStatus(Payment.Status.CONFIRMED);
                break;
            case NOT_CONFIRMED:
                out.setStatus(Payment.Status.NOT_CONFIRMED);
                break;
            case NOT_PAID:
                out.setStatus(Payment.Status.NOT_PAID);
                break;
        }
        return out;
    }
}
