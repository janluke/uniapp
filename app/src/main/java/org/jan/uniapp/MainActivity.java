package org.jan.uniapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.jan.uniapp.domain.Student;
import org.jan.uniapp.fragments.AboutFragment;
import org.jan.uniapp.fragments.CourseListFragment;
import org.jan.uniapp.fragments.ExamListFragment;
import org.jan.uniapp.fragments.PaymentsFragment;
import org.jan.uniapp.fragments.PrevisionsFragment;
import org.jan.uniapp.fragments.SettingsFragment;
import org.jan.uniapp.fragments.SummaryFragment;
import org.jan.uniapp.logic.LogicFacade;
import org.jan.uniapp.logic.UpdateTask;
import org.jan.uniapp.repository.DatabaseHelper;
import org.jan.uniapp.services.UpdateService;
import org.jan.uniapp.utils.Utils;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, UpdateTask.DbUpdaterListener {
    private final static String TAG = MainActivity.class.getSimpleName();
    private Toolbar toolbar;
    private boolean menuItemSelected;
    private NavigationView mNavigationView;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //scheduleAlarm();//TODO FIX
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        if (savedInstanceState == null) {
            inflateMainFragment();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        renderHeader(mNavigationView.getHeaderView(0));
    }

    private void inflateMainFragment() {
        this.menuItemSelected = false;
        Menu menu = mNavigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            if (menu.getItem(i).isChecked()) {
                menu.getItem(i).setChecked(false);
            }
        }
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main, SummaryFragment.newIstance())
                .commit();
    }


    private void renderHeader(View navHeader) {
        Log.v(TAG, "Set student info in menu header");
        TextView name = (TextView) navHeader.findViewById(R.id.nav_header_name);
        TextView mat = (TextView) navHeader.findViewById(R.id.nav_header_mat);
        ImageView photo = (ImageView) navHeader.findViewById(R.id.nav_header_photo);
        DatabaseHelper dbh = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        Student stu = null;
        try {
            stu = dbh.getStudent();
        } catch (SQLException e) {
            Log.e(TAG, "getting student", e);
            e.printStackTrace();
        }

        SharedPreferences sp = android.preference.PreferenceManager.getDefaultSharedPreferences(this);
        if (sp.getBoolean("SHOW_PHOTO", true)) {
            photo.setImageBitmap(Utils.getCircularBitmapFromBlob(stu.getPhotoBlob()));
        } else {
            photo.setImageResource(R.drawable.app_logo);
        }
        name.setText(stu.getName() + " " + stu.getSurname());
        mat.setText(stu.getMatricola());

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (menuItemSelected) {
                inflateMainFragment();
            } else {
                moveTaskToBack(true);
            }
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        this.menuItemSelected = true;
        int id = item.getItemId();
        android.support.v4.app.Fragment f=null;
        if (id == R.id.nav_courses) {
            Log.v(TAG, "Courses list selected");
            f=  CourseListFragment.newInstance();
        } else if (id == R.id.nav_exams) {
            Log.v(TAG, "Exam list selected");
            f = ExamListFragment.newInstance();
        } else if (id == R.id.nav_prevision) {
            Log.v(TAG, "Prevision selected");
            f = PrevisionsFragment.newInstance();
        } else if (id == R.id.nav_payment) {
            Log.v(TAG, "PaymentsFragment selected");
            f = PaymentsFragment.newInstance();
        } else if (id == R.id.nav_settings) {
            Log.v(TAG, "Logout selected");
            f = SettingsFragment.newInstance();
        } else if (id == R.id.nav_about) {
            Log.v(TAG, "About selected");
            f = AboutFragment.newInstance();
        } else if (id == R.id.nav_logout) {
            doLogout();
            return false;//affinche non resti selezionato e il menu resti aperto
        }
        if(f!=null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_main , f)
                    .commit();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void doLogout() {
        final Intent intent = new Intent(this, LoginActivity.class);

        Log.v(TAG, "Logout dialog open");
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_warning)
                .setTitle("Logout")
                .setMessage("Cosi facendo perderai tutti i tuoi dati,\nvuoi procedere?")
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i(TAG, "do logout");
                        LogicFacade.logout(getBaseContext());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void onUpdateComplete(boolean success, String cause) {
        SwipeRefreshLayout srl = ((SwipeRefreshLayout) findViewById(R.id.fragment_list));
        if (srl != null) {
            srl.setRefreshing(false);
        }
        if (!success) {
            Toast.makeText(this, "Errore nel aggiornamento\n" + cause, Toast.LENGTH_SHORT).show();
        } else {
            String data = (new SimpleDateFormat("HH:mm 'del' dd/MM/yyy")).format(new Date());
            Toast.makeText(this, "Aggiornato alle " + data, Toast.LENGTH_SHORT).show();
        }
    }

    //TODO FIX, it doesn't work
    public void scheduleAlarm() {
        // Construct an intent that will execute the AlarmReceiver
        Intent intent = new Intent(getApplicationContext(), UpdateService.MyBroadcastReceiver.class);
        // Create a PendingIntent to be triggered when the alarm goes off
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Setup periodic alarm every 5 seconds
        long firstMillis = System.currentTimeMillis(); // alarm is set right away
        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        // First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
        // Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY
        alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES, pIntent);
    }


}
