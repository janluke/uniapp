package org.jan.uniapp.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by jan on 10/03/17.
 */
@DatabaseTable(tableName = "EXAM")
public class Exam {
    @DatabaseField(id = true)
    private Long id;
    @DatabaseField(canBeNull = false)
    private Date date;
    @DatabaseField
    private Date openBookingDate;
    @DatabaseField
    private Date closeBookingData;
    @DatabaseField
    private String courseName;//todo: relation using course code
    @DatabaseField
    private String courseCode;//come sopra
    @DatabaseField
    private Integer bookedStudents;
    @DatabaseField
    private String description;
    @DatabaseField
    private String location;
    @DatabaseField
    private String teacher;
    @DatabaseField
    private Boolean booked;
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private Result result;
    ///

    public Exam() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getOpenBookingDate() {
        return openBookingDate;
    }

    public void setOpenBookingDate(Date openBookingDate) {
        this.openBookingDate = openBookingDate;
    }

    public Date getCloseBookingData() {
        return closeBookingData;
    }

    public void setCloseBookingData(Date closeBookingData) {
        this.closeBookingData = closeBookingData;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Integer getBookedStudents() {
        return bookedStudents;
    }

    public void setBookedStudents(Integer bookedStudents) {
        this.bookedStudents = bookedStudents;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getBooked() {
        return booked;
    }

    public void setBooked(Boolean booked) {
        this.booked = booked;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Exam exam = (Exam) o;

        return id != null ? id.equals(exam.id) : exam.id == null;

    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }
}
