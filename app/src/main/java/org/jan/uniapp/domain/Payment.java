package org.jan.uniapp.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by Gianluca on 12/05/2017.
 */
@DatabaseTable(tableName = "PAYMENT")
public class Payment {
    public enum Status {CONFIRMED, NOT_CONFIRMED, NOT_PAID}

    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField
    private Status status;
    @DatabaseField
    private String code;
    @DatabaseField
    private String description;
    @DatabaseField
    private Date dueDate;
    @DatabaseField
    private Double value;

    public Payment() {
        super();
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
