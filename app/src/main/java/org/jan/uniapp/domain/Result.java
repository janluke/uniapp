package org.jan.uniapp.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by jan on 10/03/17.
 */

@DatabaseTable(tableName = "RESULT")
public class Result {
    public Result() {
    }


    public enum ResultStatus{
        PENDING, ACCEPTED, REFUSED
    }
    @DatabaseField(generatedId = true)
    private Long id;
    @DatabaseField
    private Integer value;
    @DatabaseField
    private ResultStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }


    public ResultStatus getStatus() {
        return status;
    }

    public void setStatus(ResultStatus status) {
        this.status = status;
    }
}
