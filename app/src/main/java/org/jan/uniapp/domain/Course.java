package org.jan.uniapp.domain;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by jan on 10/03/17.
 */
@DatabaseTable(tableName = "COURSE")
public class Course {
    @DatabaseField(id = true)
    private long id;
    @DatabaseField(columnName = "CODE")
    private String code;
    @DatabaseField
    private String name;
    @DatabaseField
    private Integer cfu;
    @DatabaseField
    private Integer year;
    @DatabaseField
    private Integer finalResult;
    @DatabaseField
    private Date finalResultDate;

    public Course() {}


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCfu() {
        return cfu;
    }

    public void setCfu(Integer cfu) {
        this.cfu = cfu;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getFinalResult() {
        return finalResult;
    }

    public void setFinalResult(Integer finalResult) {
        this.finalResult = finalResult;
    }

    public Date getFinalResultDate() {
        return finalResultDate;
    }

    public void setFinalResultDate(Date finalResultDate) {
        this.finalResultDate = finalResultDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course course = (Course) o;

        return id == course.id;

    }

}
