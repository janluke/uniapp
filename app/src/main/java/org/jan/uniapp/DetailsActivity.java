package org.jan.uniapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import org.jan.uniapp.fragments.CourseDetailsFragment;
import org.jan.uniapp.fragments.ExamDetailsFragment;

public class DetailsActivity extends AppCompatActivity {
    private final static String TAG = DetailsActivity.class.getSimpleName();
    public static void start(Context context, String objType, long id) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra("ID", id);
        intent.putExtra("OBJ_TYPE", objType);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ///
        Intent intent = this.getIntent();
        Bundle bundle = new Bundle();
        bundle.putLong("ID", intent.getLongExtra("ID", -1));
        Fragment f = null;
        switch (intent.getStringExtra("OBJ_TYPE")){
            case "EXAM":
                Log.i(TAG, "Exam details show");
                f = new ExamDetailsFragment();
                getSupportActionBar().setTitle("Dettagli appello");
                break;
            case "COURSE":
                Log.i(TAG, "Course details show");
                f = new CourseDetailsFragment();
                getSupportActionBar().setTitle("Dettagli corso");

        }//
        f.setArguments(bundle);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main, f)
                .commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
