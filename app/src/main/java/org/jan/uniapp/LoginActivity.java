package org.jan.uniapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.jan.e3.wrapper.University;
import org.jan.uniapp.logic.LogicFacade;
import org.jan.uniapp.repository.DatabaseInitializer;

import java.io.InputStream;
import java.util.List;

/**
 * A login screen that offers login via cf/password.
 */
public class LoginActivity extends AppCompatActivity implements DatabaseInitializer.DbUpdaterListener {
    private static final String TAG = LoginActivity.class.getSimpleName();
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private DatabaseInitializer dbu;

    // UI references.
    private AutoCompleteTextView mUserView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Spinner mUniSpinner;
    private Drawable originalBackground;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mUserView = (AutoCompleteTextView) findViewById(R.id.user);
        mUniSpinner = (Spinner) findViewById(R.id.universities_spinner);
        mPasswordView = (EditText) findViewById(R.id.password);

        mUniSpinner.setAdapter(new UniSpinnerAdapter());
        mUniSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mUserView.requestFocus();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mUserView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return mPasswordView.requestFocus();

            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (dbu != null) {
            return;
        }
        if (!internet_connection()) {
            onUpdateComplete(false, "Verifica la tua connessione e riprova");
            return;
        }

        // Reset errors.
        mUserView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mUserView.getText().toString();
        String password = mPasswordView.getText().toString();
        String uni = ((University) mUniSpinner.getSelectedItem()).getName();

        boolean cancel = false;


        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mUserView.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        if (!cancel) {
            showProgress(true);
            dbu = DatabaseInitializer.getInstance(getSupportFragmentManager(), email, password, uni);
            dbu.InitializeDatabase();
        }
    }

    boolean internet_connection() {
        //Check if connected to internet, output accordingly
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {

        if (show) {
            this.originalBackground = findViewById(R.id.login_main_layout).getBackground();
            findViewById(R.id.login_main_layout).setBackground(
                    getDrawable(R.drawable.splash_screen)
            );
            this.getSupportActionBar().hide();
        } else {
            findViewById(R.id.login_main_layout).setBackground(originalBackground);
            this.getSupportActionBar().show();
        }
        hideSoftKeyboard();
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    @Override
    public void onUpdateComplete(boolean success, String cause) {
        dbu = null;
        if (success) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(intent);
            finish();
        } else {
            showProgress(false);
            Toast.makeText(this, cause + "!!!", Toast.LENGTH_LONG).show();
        }
    }


    private class UniSpinnerAdapter extends BaseAdapter {
        List<University> unis;

        public UniSpinnerAdapter() {
            super();
            unis = LogicFacade.getFactory(getBaseContext()).getAvviableUniversities();
        }

        @Override
        public int getCount() {
            return unis.size();
        }

        @Override
        public Object getItem(int position) {
            return unis.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.uni_spinner_element, null, false);
            }
            TextView name = ((TextView) view.findViewById(R.id.full_uni_name));
            ImageView logo = (ImageView) view.findViewById(R.id.uni_logo_view);
            University uni = ((University) getItem(position));

            loadLogo(logo, uni.getName().toLowerCase());
            name.setText(uni.getName());
            name.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            name.setTypeface(null, Typeface.BOLD);
            name.setTextSize(20);
            return view;
        }

        private void loadLogo(ImageView imageView, String name) {
            try {
                InputStream is = getResources().openRawResource(
                        getResources().getIdentifier(name,
                                "raw", getPackageName()));
                imageView.setImageBitmap(BitmapFactory.decodeStream(is));
            } catch (Exception e) {
                Log.e(TAG, "loading uni logo", e);
            }
        }
    }

}

