package org.jan.uniapp.logic;

import android.os.AsyncTask;

import org.jan.e3.StudentFacade;
import org.jan.e3.domain.Exam;
import org.jan.e3.utils.E3PException;

import java.util.List;

/**
 * Created by jan on 29/03/17.
 */

public class JoinAsyncTask extends AsyncTask<String, Void, String> {
    public static final String[] actions = {"JOIN", "UNJOIN"};
    public interface JoinTaskListener {
        void onJoinTaskComplete(String[] params, String erro);
    }

    private final JoinTaskListener listener;
    private String[] parms;
    private StudentFacade studentFacade;

    public JoinAsyncTask(JoinTaskListener listener) {
        this.listener = listener;
    }


    @Override
    protected String doInBackground(String... params) {
        parms = params;
        String action = params[0], code = params[1];
        boolean matched = false;
        try {
            List<Exam> es = studentFacade.getAvviableExams();
            es.addAll(studentFacade.getPrenotedExams());
            for (org.jan.e3.domain.Exam e : es) {
                String hash = String.valueOf(e.hashCode());
                if (hash.equals(code)) {
                    matched = true;
                    switch (action) {
                        case "JOIN":
                            studentFacade.join(e);
                            break;
                        case "UNJOIN":
                            studentFacade.unjoin(e);
                            break;
                    }
                }
            }
        } catch (E3PException e) {
            e.printStackTrace();
            return e.getType().toString();
        }
        return matched ? null : "Exam not found";
    }


    void setStudentFacade(StudentFacade studentFacade) {
        this.studentFacade = studentFacade;
    }

    @Override
    protected void onPostExecute(String error) {
        super.onPostExecute(error);
        listener.onJoinTaskComplete(parms, error);
    }
}
