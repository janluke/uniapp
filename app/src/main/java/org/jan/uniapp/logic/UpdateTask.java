package org.jan.uniapp.logic;

import android.os.AsyncTask;
import android.util.Log;

import com.j256.ormlite.dao.Dao;

import org.jan.e3.StudentFacade;
import org.jan.e3.domain.Course;
import org.jan.e3.domain.Exam;
import org.jan.e3.domain.Payment;
import org.jan.e3.utils.E3PException;
import org.jan.uniapp.repository.DatabaseHelper;
import org.jan.uniapp.utils.DomainFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by jan on 29/03/17.
 */


public class UpdateTask extends AsyncTask<String, Void, String> {
    private static final String TAG = UpdateTask.class.getSimpleName();
    private DbUpdaterListener listener;
    private StudentFacade uni;
    private DatabaseHelper dbHelper;

    public interface DbUpdaterListener {
        void onUpdateComplete(boolean success, String cause);
    }

    public UpdateTask(DbUpdaterListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (listener != null)
            listener.onUpdateComplete(s == null, s);
    }

    @Override
    protected String doInBackground(String... params) {
        try {

            List<Exam> es = uni.getAvviableExams();
            es.addAll(uni.getPrenotedExams());
            updateCourse(uni.getCourses());
            updateExams(es);
            updatePayments(uni.getPayments());
            return null;
        } catch (E3PException e) {
            Log.e(TAG, "Interrogating remote student area");
            return e.getMessage();
        }
    }

    public void setUni(StudentFacade uni) {
        this.uni = uni;
    }

    public void setDbHelper(DatabaseHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    private void updateExams(final List<Exam> exams) {
        Dao<org.jan.uniapp.domain.Exam, Long> dao = null;
        try {
            dao = dbHelper.getExamDao();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(getClass().getName(), "Init exam dao", e);
        }
        final Dao<org.jan.uniapp.domain.Exam, Long> finalDao = dao;

        try {
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (Exam e : exams) {
                        finalDao.createOrUpdate(DomainFactory.getInstance(e));
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Updating exams", e);
        }

    }

    private void updateCourse(final List<Course> courses) {
        Dao<org.jan.uniapp.domain.Course, Long> dao = null;
        try {
            dao = dbHelper.getCourseDao();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(getClass().getName(), "Init course dao", e);
        }


        final ArrayList<org.jan.uniapp.domain.Course> cs = new ArrayList<>();


        final Dao<org.jan.uniapp.domain.Course, Long> finalDao = dao;
        try {
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (Course c : courses) {
                        finalDao.createOrUpdate(DomainFactory.getInstance(c));
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Updating Courses", e);
        }


    }

    private void updatePayments(final List<Payment> payments) {
        Dao<org.jan.uniapp.domain.Payment, Long> dao = null;
        try {
            dao = dbHelper.getPaymentsDao();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(getClass().getName(), "Init payments dao", e);
        }
        final List<org.jan.uniapp.domain.Payment> ps = new ArrayList<>();
        try {
            final Dao<org.jan.uniapp.domain.Payment, Long> finalDao = dao;
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (Payment p : payments) {
                        finalDao.createOrUpdate(DomainFactory.getInstance(p));
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Updating Payments", e);
        }
    }


}



