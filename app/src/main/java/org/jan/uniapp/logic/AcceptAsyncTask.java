package org.jan.uniapp.logic;

import android.os.AsyncTask;

import org.jan.e3.StudentFacade;
import org.jan.e3.domain.Exam;
import org.jan.e3.utils.E3PException;

import java.util.List;

/**
 * Created by jan on 29/03/17.
 */

public class AcceptAsyncTask extends AsyncTask<String, Void, String> {
    public static final String[] actions = {"ACCEPT", "REFUSE"};

    public interface AcceptTaskListener {
        void onAcceptTaskComplete(String[] params, String erro);
    }

    private String[] params;
    private StudentFacade studentFacade;
    private AcceptTaskListener listener;

    public AcceptAsyncTask(AcceptTaskListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {
        this.params = params;
        String action = params[0], code = params[1];
        boolean matched = false;
        try {
            List<Exam> es = studentFacade.getAvviableExams();
            es.addAll(studentFacade.getPrenotedExams());
            for (org.jan.e3.domain.Exam e : es) {
                String hash = String.valueOf(e.hashCode());
                if (hash.equals(code)) {
                    matched = true;
                    //TODO: FIX AFTER BACK-END IMPLEMENTETION
                    switch (action) {
                        case "ACCEPT":
                            studentFacade.accept(e.getResult());
                            break;
                        case "REFUSE":
                            studentFacade.refuse(e.getResult());
                            break;
                    }
                }
            }
        } catch (E3PException e) {
            e.printStackTrace();
            return e.getType().toString();
        }
        return matched ? null : "Exam not found";
    }

    @Override
    protected void onPostExecute(String error) {
        super.onPostExecute(error);
        listener.onAcceptTaskComplete(params, error);

    }

    void setStudentFacade(StudentFacade studentFacade) {
        this.studentFacade = studentFacade;
    }
}
