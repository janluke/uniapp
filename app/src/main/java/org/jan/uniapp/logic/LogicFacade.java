package org.jan.uniapp.logic;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;

import org.jan.e3.StudentFacade;
import org.jan.e3.utils.E3PException;
import org.jan.e3.wrapper.FacadeFactory;
import org.jan.e3.wrapper.University;
import org.jan.uniapp.R;
import org.jan.uniapp.domain.Exam;
import org.jan.uniapp.domain.Student;
import org.jan.uniapp.repository.DatabaseHelper;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

/**
 * Created by jan on 28/03/17.
 */

public class LogicFacade {
    private static StudentFacade studentFacade;
    private static final String TAG = LogicFacade.class.getSimpleName();
    private static FacadeFactory facadeFactory;

    public static StudentFacade getInstance(Context context, String name, String pass, String university) throws IOException {
        getFactory(context).setPassword(pass).setUsername(name);
        if (university == null) {
            facadeFactory.setUniversity(facadeFactory.getAvviableUniversities().get(0));
        } else {
            for (University u : facadeFactory.getAvviableUniversities()) {
                if (u.getName().equals(university)) facadeFactory.setUniversity(u);
            }
        }
        try {
            studentFacade = facadeFactory.getFacade();
        } catch (E3PException e) {
            Log.e(LogicFacade.class.getSimpleName(), "Making facade", e);
            e.printStackTrace();
        }
        return studentFacade;
    }

    public static StudentFacade getInstance(Context context) throws IOException {
        if (studentFacade == null) {
            DatabaseHelper dbh = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            Student stu = null;
            try {
                stu = dbh.getStudent();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (stu != null) {
                return getInstance(context, stu.getCf(), stu.getPassword(), stu.getUniversity());
            }
        }
        return studentFacade;
    }


    public static FacadeFactory getFactory(Context context) {
        if (facadeFactory == null) {
            try {
                InputStream in_s = context.getResources().openRawResource(R.raw.e3p);
                byte[] conf = new byte[in_s.available()];
                in_s.read(conf);
                facadeFactory = (new FacadeFactory(new String(conf)));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return facadeFactory;
    }


    public static void join(Context context, JoinAsyncTask.JoinTaskListener listener, Exam exam) {
        JoinAsyncTask jat = new JoinAsyncTask(listener);
        try {
            jat.setStudentFacade(getInstance(context));
        } catch (IOException e) {
            Log.e(TAG, "Joining ", e);
            e.printStackTrace();
        }
        jat.execute("JOIN", exam.getId().toString());
        LogicFacade.Update(context, null);
    }

    public static void unjoin(Context context, JoinAsyncTask.JoinTaskListener listener, Exam exam) {
        JoinAsyncTask jat = new JoinAsyncTask(listener);
        try {
            jat.setStudentFacade(getInstance(context));
        } catch (IOException e) {
            Log.e(TAG, "Unjoing", e);
            e.printStackTrace();
        }
        jat.execute("UNJOIN", exam.getId().toString());
        LogicFacade.Update(context, null);
    }

    public static void accept(Context context, AcceptAsyncTask.AcceptTaskListener listener, Exam exam) {
        AcceptAsyncTask aat = new AcceptAsyncTask(listener);
        try {
            aat.setStudentFacade(getInstance(context));
        } catch (IOException e) {
            e.printStackTrace();
        }
        aat.execute("ACCEPT", exam.getId().toString());
        LogicFacade.Update(context, null);
    }

    public static void refuse(Context context, AcceptAsyncTask.AcceptTaskListener listener, Exam exam) {
        AcceptAsyncTask aat = new AcceptAsyncTask(listener);
        try {
            aat.setStudentFacade(getInstance(context));
        } catch (IOException e) {
            e.printStackTrace();
        }
        aat.execute("REFUSE", exam.getId().toString());
        LogicFacade.Update(context, null);
    }




    public static void Update(Context context, UpdateTask.DbUpdaterListener listener) {
        UpdateTask ut = new UpdateTask(listener);
        ut.setDbHelper(OpenHelperManager.getHelper(context, DatabaseHelper.class));
        try {
            ut.setUni(getInstance(context));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ut.execute();
    }

    public static void logout(Context context) {
        DatabaseHelper dbh = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        dbh.cleanAll();
    }
}
