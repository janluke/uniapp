package org.jan.uniapp.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import org.jan.uniapp.domain.Course;
import org.jan.uniapp.domain.Exam;
import org.jan.uniapp.domain.Payment;
import org.jan.uniapp.domain.Result;
import org.jan.uniapp.domain.Student;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by jan on 12/03/17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "uniapp.db";

    private static final int DATABASE_VERSION = 12;
    private Dao<Course, Long> courseDao;
    private Dao<Student, Long> studentDao;
    private Dao<Exam,Long> examDao;
    private Dao<Payment, Long> paymentsDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Course.class);
            TableUtils.createTableIfNotExists(connectionSource, Student.class);
            TableUtils.createTableIfNotExists(connectionSource, Exam.class);
            TableUtils.createTableIfNotExists(connectionSource, Result.class);
            TableUtils.createTableIfNotExists(connectionSource, Payment.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Course.class, true);
            TableUtils.dropTable(connectionSource, Student.class, true);
            TableUtils.dropTable(connectionSource, Exam.class, true);
            TableUtils.dropTable(connectionSource, Result.class, true);
            TableUtils.dropTable(connectionSource, Payment.class, true);

            Log.d(getClass().getName(), "tables dropped");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        onCreate(database, connectionSource);
    }

    public Dao<Course, Long> getCourseDao() throws SQLException {
        if (courseDao == null) {
            courseDao = getDao(Course.class);
        }
        return courseDao;
    }

    public Dao<Exam,Long> getExamDao() throws SQLException {
        if (examDao == null) {
            examDao = getDao(Exam.class);
        }
        return examDao;
    }
    public Student getStudent() throws SQLException {
        List<Student> list = getStudentDao().queryForAll();
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }

    void setStudent(Student student) throws SQLException {
        getStudentDao().createOrUpdate(student);
    }

    Dao<Student, Long> getStudentDao() throws SQLException {
        if (studentDao == null) {
            studentDao = getDao(Student.class);
        }
        return studentDao;
    }

    public Dao<Payment, Long> getPaymentsDao() throws SQLException {
        if (paymentsDao == null) {
            paymentsDao = getDao(Payment.class);
        }
        return paymentsDao;
    }

    public void cleanAll() {
        try {
            TableUtils.clearTable(connectionSource, Course.class);
            TableUtils.clearTable(connectionSource, Student.class);
            TableUtils.clearTable(connectionSource, Exam.class);
            TableUtils.clearTable(connectionSource, Result.class);
            TableUtils.clearTable(connectionSource, Payment.class);
            Log.d(getClass().getName(), "tables cleaned");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}