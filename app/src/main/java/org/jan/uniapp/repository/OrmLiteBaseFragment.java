package org.jan.uniapp.repository;

import android.support.v4.app.Fragment;

import com.j256.ormlite.android.apptools.OpenHelperManager;


/**
 * Created by jan on 13/03/17.
 */
public abstract class OrmLiteBaseFragment extends Fragment {

    private static DatabaseHelper databaseHelper;

    protected DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper =
                    OpenHelperManager.getHelper(getActivity(), DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }

    }
}
