package org.jan.uniapp.repository;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import org.jan.uniapp.domain.Course;

/**
 * Created by jan on 12/03/17.
 */
public class DatabaseConfigUtil extends OrmLiteConfigUtil {
    private static final Class<?>[] classes = new Class[] {
           Course.class,// Exam.class, Result.class, Student.class// TODO:quando funziona completa le altre classi
    };
    public static void main(String[] args) throws Exception {
        writeConfigFile("DB_config.txt", classes);
    }
}
