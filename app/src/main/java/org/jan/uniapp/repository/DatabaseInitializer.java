package org.jan.uniapp.repository;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.j256.ormlite.dao.Dao;

import org.jan.e3.StudentFacade;
import org.jan.e3.domain.Course;
import org.jan.e3.domain.Exam;
import org.jan.e3.utils.E3PException;
import org.jan.uniapp.domain.Student;
import org.jan.uniapp.logic.LogicFacade;
import org.jan.uniapp.utils.DomainFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class DatabaseInitializer extends OrmLiteBaseFragment {
    public static final String TAG = DatabaseInitializer.class.getSimpleName();
    private DbUpdaterListener listener;
    private UpdateTask mUpdateTask;
    private String user, pass, university;


    public static DatabaseInitializer getInstance(FragmentManager fragmentManager, String user, String pass, String university) {
        DatabaseInitializer fragment = new DatabaseInitializer();
        try {
            fragment.setCredetials(user, pass, university);
        } catch (Exception e) {
        }
        fragmentManager.beginTransaction().add(fragment, TAG).commit();
        return fragment;
    }


    private void setCredetials(String user, String pass, String university) throws Exception {
        if(user==null&pass==null){
            Student stu = null;
            try {
                stu = getHelper().getStudent();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if(stu!=null){
                user = stu.getCf();
                pass = stu.getPassword();
            }else{
                throw new Exception("NO LOGGED USER");
            }
        }
        this.user = user;
        this.pass = pass;
        this.university = university;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Host Activity will handle callbacks from task.
        listener= (DbUpdaterListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // Clear reference to host Activity to avoid memory leak.
        listener = null;
    }


    /**
     * Start non-blocking execution of updateTask.
     */
    public void InitializeDatabase() {
        this.mUpdateTask = new UpdateTask();
        mUpdateTask.execute(user, pass, university);
    }


    public interface DbUpdaterListener {
        void onUpdateComplete(boolean success,String cause);
    }
    private class UpdateTask extends AsyncTask<String,Void,String>{

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            listener.onUpdateComplete(s==null,s);
        }

        @Override
        protected String doInBackground(String... params) {
            StudentFacade uni = null;
            try {
                uni = LogicFacade.getInstance(getContext(), user, pass, university);
                updateStudent(uni.getStudent());
                updateCourse(uni.getCourses());
                updateExams(uni.getAvviableExams());
                updateResults(uni.getPrenotedExams());
                return null;
            } catch (E3PException  e) {
                Log.e(TAG,"Interrogating remote student area");
                return e.getMessage();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private void updateResults(List<Exam> prenotedExams) {
            updateExams(prenotedExams);

        }


        private void updateExams(List<Exam> exams) {
            Dao<org.jan.uniapp.domain.Exam,Long> dao = null;
            try {
                dao = getHelper().getExamDao();
            } catch (SQLException e) {
                e.printStackTrace();
                Log.e(getClass().getName(),"Init exam dao",e);
            }

            ArrayList<org.jan.uniapp.domain.Exam> es = new ArrayList<>();
            for(Exam e :exams){
                es.add(DomainFactory.getInstance(e));
                
            }
            try {
                dao.create(es);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        private void updateCourse(List<Course> courses)  {
            Dao<org.jan.uniapp.domain.Course,Long> dao = null;
            try {
                dao = getHelper().getCourseDao();
            } catch (SQLException e) {
                e.printStackTrace();
                Log.e(getClass().getName(),"Init course dao",e);
            }

            try {
                dao.delete(dao.queryForAll());
            } catch (SQLException e) {
                e.printStackTrace();

                Log.e(getClass().getName(),"getting courses from local database",e);
            }

            ArrayList<org.jan.uniapp.domain.Course> cs = new ArrayList<>();

            for(Course c:courses){
                cs.add(DomainFactory.getInstance(c));
            }

            try {
                dao.create(cs);
            } catch (SQLException e) {
                e.printStackTrace();
                Log.e(getClass().getName(),"Updating courses",e);
            }

        }

        private void updateStudent(org.jan.e3.domain.Student student) {
            try {
                Student stu = DomainFactory.getInstance(student);
                stu.setCf(user);
                stu.setPassword(pass);
                stu.setUniversity(university);
                getHelper().setStudent(stu);
            } catch (SQLException e) {
                e.printStackTrace();
                Log.e(getClass().getName(),"Updating student info",e);
            }

        }

    }


}
