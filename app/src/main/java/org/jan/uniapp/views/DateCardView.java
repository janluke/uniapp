package org.jan.uniapp.views;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.TextView;

import org.jan.uniapp.R;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by jan on 31/03/17.
 */
//TODO: includere le subview in questa classe, non nel layout
public class DateCardView extends CardView {
    private Date date;

    public DateCardView(Context context) {
        super(context);
    }

    public DateCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DateCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
        render();
    }

    private void render() {
        TextView month = (TextView) this.findViewById(R.id.exam_details_month);
        TextView day = (TextView) this.findViewById(R.id.exam_details_day);
        TextView year = (TextView) this.findViewById(R.id.exam_details_year);
        day.setText(new SimpleDateFormat("dd").format(date));
        year.setText(new SimpleDateFormat("yyyy").format(date));
        month.setText(getMonth(date));
    }

    private static String getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return new DateFormatSymbols(Locale.ITALY).getMonths()[cal.get(Calendar.MONTH)];
    }
}
