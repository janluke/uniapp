package org.jan.uniapp.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.jan.e3.StudentFacade;
import org.jan.e3.domain.Course;
import org.jan.e3.domain.Exam;
import org.jan.e3.utils.E3PException;
import org.jan.uniapp.R;
import org.jan.uniapp.logic.LogicFacade;
import org.jan.uniapp.repository.DatabaseHelper;
import org.jan.uniapp.utils.DomainFactory;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;


public class UpdateService extends IntentService {
    private static final String TAG = UpdateService.class.getSimpleName();
    private StudentFacade uni;
    private DatabaseHelper dbHelper;

    public UpdateService() {
        super("UpdateService");
        try {
            uni = LogicFacade.getInstance(getBaseContext());
            dbHelper = OpenHelperManager.getHelper(getBaseContext(), DatabaseHelper.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        List<Exam> es = null;
        try {
            es = uni.getAvviableExams();
            es.addAll(uni.getPrenotedExams());
            updateCourse(uni.getCourses());
            updateExams(es);
        } catch (E3PException e) {
            e.printStackTrace();
            Log.e(TAG, "Handling intent", e);
        }
    }

    private void updateExams(final List<Exam> exams) {
        Dao<org.jan.uniapp.domain.Exam, Long> dao = null;
        try {
            dao = dbHelper.getExamDao();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(getClass().getName(), "Init exam dao", e);
        }
        final Dao<org.jan.uniapp.domain.Exam, Long> finalDao = dao;
        final ArrayList<org.jan.uniapp.domain.Exam> es = new ArrayList<>();
        for (Exam e : exams) {
            es.add(DomainFactory.getInstance(e));
        }
        try {
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    List<org.jan.uniapp.domain.Exam>
                            olds = finalDao.queryForAll(),
                            youngs = new ArrayList<>(es),//copia dei nuovi esami
                            newVoted = new ArrayList<>();

                    ///rimuoviamo dalla lista young quelli gia presenti nel db
                    Iterator<org.jan.uniapp.domain.Exam> it = youngs.iterator();
                    while (it.hasNext()) {
                        org.jan.uniapp.domain.Exam young = it.next();
                        for (org.jan.uniapp.domain.Exam old : olds) {
                            if (young.equals(old)) {
                                if (old.getBooked()) {
                                    if ((old.getResult() == null) & (young.getResult() != null)) {
                                        newVoted.add(young);
                                    }
                                }
                                it.remove();
                            }
                        }
                    }
                    if (!youngs.isEmpty()) {
                        handleNewExamPublished(youngs);
                    }
                    if (!newVoted.isEmpty()) {
                        handleNewResultPublished(newVoted);
                    }
                    for (org.jan.uniapp.domain.Exam e : es) {
                        finalDao.createOrUpdate(e);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Updating exams", e);
        }

    }

    private void updateCourse(List<Course> courses) {
        Dao<org.jan.uniapp.domain.Course, Long> dao = null;
        try {
            dao = dbHelper.getCourseDao();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(getClass().getName(), "Init course dao", e);
        }
        final ArrayList<org.jan.uniapp.domain.Course> cs = new ArrayList<>();

        for (Course c : courses) {
            cs.add(DomainFactory.getInstance(c));
        }
        final Dao<org.jan.uniapp.domain.Course, Long> finalDao = dao;
        try {
            dao.callBatchTasks(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    List<org.jan.uniapp.domain.Course>
                            olds = finalDao.queryForAll(),
                            youngs = (new ArrayList<>(cs));

                    ///rimuoviamo dalla lista young quelli gia presenti nel db
                    Iterator<org.jan.uniapp.domain.Course> it = youngs.iterator();
                    while (it.hasNext()) {
                        org.jan.uniapp.domain.Course young = it.next();
                        for (org.jan.uniapp.domain.Course old : olds) {
                            if (young.equals(old)) {
                                it.remove();
                            }
                        }
                    }
                    if (!youngs.isEmpty()) {
                        handleNewCoursePublished(youngs);
                    }
                    Log.v(TAG, "Create or update courses");

                    for (org.jan.uniapp.domain.Course c : cs) {
                        finalDao.createOrUpdate(c);
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Updating Courses", e);
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleNewResultPublished(List<org.jan.uniapp.domain.Exam> examsWithNewResult) {
        // TODO: Notifica i risultati publicati
        String title, msg;
        if (examsWithNewResult.size() > 1) {
            title = "Risultati publicati";
            msg = examsWithNewResult.size() + " " + "nuovi risultati";
        } else {
            org.jan.uniapp.domain.Exam ex = examsWithNewResult.get(0);

            title = "Risultato publicato";
            msg = ex.getResult().getValue() + "-" + ex.getCourseName();
        }
        createNotification(42, title, msg);
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleNewExamPublished(List<org.jan.uniapp.domain.Exam> newExams) {
        // TODO: Notifica i nuovi esami publicati
        String title, msg;
        if (newExams.size() > 1) {
            title = "Nuovi appelli disponibili";
            msg = newExams.size() + " " + "nuovi appelli";
        } else {
            org.jan.uniapp.domain.Exam ex = newExams.get(0);

            title = "Nuovo appello disponibile";
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
            msg = sdf.format(ex.getDate()) + "-" + ex.getCourseName();
        }
        createNotification(45, title, msg);

    }

    private void handleNewCoursePublished(List<org.jan.uniapp.domain.Course> newCourses) {
        // TODO: Notifica i nuovi corsi publicati?
    }

    private void createNotification(int nId, String title, String body) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.bookable_exam)
                .setContentTitle(title)
                .setContentText(body);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(nId, mBuilder.build());
    }

    public class MyBroadcastReceiver extends BroadcastReceiver {

        // Triggered by the Alarm periodically (starts the service to run task)
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent i = new Intent(context, UpdateService.class);
            context.startService(i);
        }
    }

}
