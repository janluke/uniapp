package org.jan.uniapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.jan.uniapp.R;
import org.jan.uniapp.domain.Course;
import org.jan.uniapp.repository.OrmLiteBaseFragment;
import org.jan.uniapp.utils.SummaryUtil;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SummaryFragment extends OrmLiteBaseFragment {
    private final static String TAG = SummaryFragment.class.getSimpleName();
    private SummaryUtil summaryUtil;

    public SummaryFragment() {
    }

    public static SummaryFragment newIstance() {
        return new SummaryFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_summary, container, false);
        summaryUtil = new SummaryUtil(getSignificatCourses());

        ViewPager pager = (ViewPager) view.findViewById(R.id.charts_pager);
        TextView averange = (TextView) view.findViewById(R.id.averange_vote);
        TextView degreeVote = (TextView) view.findViewById(R.id.base_degree_vote);
        TextView cfuOnTotal = (TextView) view.findViewById(R.id.cfu_on_total);
        ProgressBar cfuGained = (ProgressBar) view.findViewById(R.id.cfu_progress);
        NestedScrollView scrollView = (NestedScrollView) view.findViewById(R.id.summary_nested_scroll);
        scrollView.setFillViewport(true);

        pager.setAdapter(new ChartsViewPagerAdapter(getChildFragmentManager()));

        String avStr = String.valueOf(summaryUtil.getAverange());
        averange.setText(avStr.length() > 4 ? avStr.substring(0, 5) : avStr);

        String deStr = String.valueOf(summaryUtil.getDegreeVote());
        degreeVote.setText(deStr.length() > 4 ? deStr.substring(0, 5) : deStr);

        cfuOnTotal.setText(
                (summaryUtil.getGainedCfu()) + "/" + (summaryUtil.getTotalCfu()) + " CFU");
        cfuGained.setMax(summaryUtil.getTotalCfu());
        cfuGained.setProgress(summaryUtil.getGainedCfu());
        return view;
    }

    //TODO A QUERY iNSTEAD OF METHOD
    private List<Course> getSignificatCourses() {
        List<Course> courses = null;
        try {
            courses = getHelper().getCourseDao().queryForAll();
            Iterator<Course> it = courses.iterator();
            while (it.hasNext()) {
                Course c = it.next();
                if (c.getCfu() == 0) {
                    it.remove();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courses;
    }

    private class ChartsViewPagerAdapter extends FragmentPagerAdapter {

        private final int PIE_CHART = 1, COMBINED_CHART = 0;
        private final int CHARTS_COUNT = 2;

        public ChartsViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case COMBINED_CHART:
                    return AverangeChartFragment.newIstance();
                case PIE_CHART:
                    return PieVoteChartFragment.newIstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return CHARTS_COUNT;
        }
    }
}
