package org.jan.uniapp.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.MPPointF;

import org.jan.uniapp.R;
import org.jan.uniapp.domain.Course;
import org.jan.uniapp.repository.OrmLiteBaseFragment;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Gianluca on 06/04/2017.
 */

public class PieVoteChartFragment extends OrmLiteBaseFragment {
    private final static String TAG = PieVoteChartFragment.class.getSimpleName();
    private int count;

    public static PieVoteChartFragment newIstance() {
        return new PieVoteChartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mInflater.inflate(R.layout.piechart_layout, null, true);
        render(view);
        return view;
    }

    private void render(View view) {

        TextView completed = (TextView) view.findViewById(R.id.count_completed_courses);
        PieChart pieChart = (PieChart) view.findViewById(R.id.votes_pie_chart);

        PieData data = getChartData();


        completed.setText(String.valueOf(this.count));

        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(Typeface.DEFAULT_BOLD);

        pieChart.setData(data);

        pieChart.getLegend().setEnabled(false);
        Description d = new Description();
        d.setEnabled(false);

        pieChart.setDescription(d);
    }

    private PieData getChartData() {
        List<Course> cs = getCourses();
        this.count = 0;
        Map<Integer, Integer> voteCounts = new HashMap<>();
        for (Course c : cs) {
            int fr = c.getFinalResult();
            if (fr != 0) {
                if (fr > 1) {
                    count++;
                    if (!voteCounts.containsKey(fr)) {
                        voteCounts.put(fr, new Integer(0));
                    }
                    voteCounts.put(fr, voteCounts.get(fr) + 1);
                }
            }
        }
        //
        ArrayList<PieEntry> pieEntries = new ArrayList<>();
        for (Integer vote : voteCounts.keySet()) {
            float percentage = voteCounts.get(vote);
            percentage *= 100.0 / count;
            String label = vote > 30 ? "30L" : String.valueOf(vote);

            pieEntries.add(new PieEntry(percentage, label));
        }
        PieDataSet dataSet = new PieDataSet(pieEntries, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);


        dataSet.setColor(getResources().getColor(R.color.colorPrimaryDark));
        dataSet.addColor(getResources().getColor(R.color.colorPrimary));
        dataSet.setSelectionShift(0f);

        return new PieData(dataSet);
    }

    public List<Course> getCourses() {
        try {
            return getHelper().getCourseDao().queryForAll();
        } catch (SQLException e) {
            Log.e(TAG, "Quering courses", e);
            e.printStackTrace();
        }
        return null;
    }
}
