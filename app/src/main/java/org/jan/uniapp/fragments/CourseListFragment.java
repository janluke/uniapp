package org.jan.uniapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.j256.ormlite.dao.Dao;

import org.jan.uniapp.DetailsActivity;
import org.jan.uniapp.R;
import org.jan.uniapp.domain.Course;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class CourseListFragment extends BaseListFragment implements AdapterView.OnItemSelectedListener {
    private final String TAG = CourseListFragment.class.getSimpleName();
    private final String[] SORT_OPTIONS = {"Nome", "Voto", "Influenza"};
    private Context parent;
    private RecyclerView recyclerView;
    private CourseRowRecyclerViewAdapter adapter;

    private Dao<Course, Long> getCourseDao() {
        try {
            return getHelper().getCourseDao();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(getClass().getName(),"Getting courses dao",e);

        }
        return null;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CourseListFragment() {
    }

    @SuppressWarnings("unused")
    public static CourseListFragment newInstance() {
        CourseListFragment fragment = new CourseListFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        this.recyclerView = (RecyclerView) view.findViewById(R.id.list_RecyclerView);
        View elm = view.findViewById(R.id.empty_list_message);

        List<Course> cList = new ArrayList<>();

        try {
            cList.addAll(getCourseDao().queryForAll());
            //metti in ordine alfabetico
            Collections.sort(cList, new Comparator<Course>() {
                @Override
                public int compare(Course o1, Course o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(getClass().getName(),"Quering courses from local db",e);
        }
        if (cList.isEmpty()) {
            showSortHeader(false);
            recyclerView.setVisibility(View.GONE);
            elm.setVisibility(View.VISIBLE);
        } else {
            showSortHeader(true);
            recyclerView.setVisibility(View.VISIBLE);
            elm.setVisibility(View.GONE);
        }
        this.adapter = new CourseRowRecyclerViewAdapter();
        adapter.setCourses(cList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        recyclerView.setAdapter(adapter);
        super.setSortOptions(SORT_OPTIONS, this);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.parent = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parent = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Comparator<Course> comparator = null;
        switch (SORT_OPTIONS[position].toLowerCase()) {
            default:
            case "nome":
                comparator = new Comparator<Course>() {
                    @Override
                    public int compare(Course c1, Course c2) {
                        return c1.getName().compareTo(c2.getName());
                    }
                };
                break;
            case "voto":
                comparator = new Comparator<Course>() {
                    @Override
                    public int compare(Course o1, Course o2) {
                        return o2.getFinalResult().compareTo(o1.getFinalResult());
                    }
                };
                break;
            case "influenza":
                comparator = new Comparator<Course>() {
                    @Override
                    public int compare(Course o1, Course o2) {
                        Double inf1 = (double) o1.getFinalResult() * (double) o1.getCfu(),
                                inf2 = (double) o2.getFinalResult() * (double) o2.getCfu();
                        return inf2.compareTo(inf1);
                    }
                };
                break;
        }
        List<Course> cs = getCourses();
        if (comparator != null && cs != null) {
            Collections.sort(cs, comparator);
            adapter.setCourses(cs);
        }
    }

    private List<Course> getCourses() {
        Dao<Course, Long> dao = getCourseDao();
        try {
            return dao.queryForAll();
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class CourseRowRecyclerViewAdapter extends RecyclerView.Adapter<CourseRowRecyclerViewAdapter.Holder> {
        private List<Course> courses;

        public void setCourses(List<Course> courses) {
            this.courses = courses;
            notifyDataSetChanged();
        }


        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_course, parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder h, int position) {
            Course c = courses.get(position);
            h.name.setText(c.getName());
            h.cfu_value.setText(String.valueOf(c.getCfu()));
            h.year.setText(String.valueOf(c.getYear()));
            renderResult(h.vote, c.getFinalResult());
        }

        public void renderResult(ImageView img, int result) {
            int color = getResources().getColor(R.color.colorAccent);
            String text = new String();
            if (result > 1) {
                text = String.valueOf(result);
                if (result > 25) {
                    color = getResources().getColor(R.color.good);
                    if (result > 30) text = "30L";
                } else if (result > 20) {
                    color = getResources().getColor(R.color.not_bad);
                } else {
                    color = getResources().getColor(R.color.bad);
                }
            } else if (result == 1) {
                text = "✓";
                color = getResources().getColor(R.color.good);
            }
            TextDrawable drawable1 = TextDrawable.builder()
                    .buildRound(text, color);
            img.setImageDrawable(drawable1);
        }

        private Course getItem(int position) {
            return courses.get(position);
        }
        @Override
        public long getItemId(int position) {
            return courses.get(position).getId();
        }

        @Override
        public int getItemCount() {
            return courses.size();
        }

        class Holder extends RecyclerView.ViewHolder {

            private final TextView cfu_value, name, year;
            private final ImageView vote;

            public Holder(View view) {
                super(view);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        long id = getItem(getAdapterPosition()).getId();
                        DetailsActivity.start(parent, "COURSE", id);
                    }
                });
                cfu_value = (TextView) view.findViewById(R.id.cfu_value);
                name = (TextView) view.findViewById(R.id.name_of_course);
                year = (TextView) view.findViewById(R.id.year_value);
                vote = (ImageView) view.findViewById(R.id.final_result_image);
            }
        }
    }
}
