package org.jan.uniapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;

import org.jan.uniapp.DetailsActivity;
import org.jan.uniapp.R;
import org.jan.uniapp.domain.Course;
import org.jan.uniapp.domain.Exam;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by jan on 15/03/17.
 */

public class ExamListFragment extends BaseListFragment implements AdapterView.OnItemSelectedListener {
    private final static String TAG = ExamListFragment.class.getSimpleName();
    private static String FILTERED_PER_COURSE = "FILTERED";
    private final String[] SORT_OPTIONS = {"Nome", "Data"};
    private Context parent;
    private ExamRowRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;

    private Dao<Exam, Long> getExamDao() {
        try {
            return getHelper().getExamDao();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(getClass().getName(),"Getting exams dao",e);

        }
        return null;
    }
    @SuppressWarnings("unused")
    public static ExamListFragment newInstance() {
        ExamListFragment fragment = new ExamListFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    public static ExamListFragment newFilteredPerCourseInstance(Course course) {
        ExamListFragment fragment = new ExamListFragment();
        Bundle args = new Bundle();
        args.putString(FILTERED_PER_COURSE, course.getCode());
        fragment.setArguments(args);
        return fragment;

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.list_RecyclerView);
        View elm = view.findViewById(R.id.empty_list_message);
        List<Exam> eList = new ArrayList<>();
        try {
            if (getArguments() != null) {
                if (getArguments().containsKey(FILTERED_PER_COURSE)) {
                    String code = getArguments().getString(FILTERED_PER_COURSE);
                    eList.addAll(
                            getExamDao().queryBuilder()
                                    .where().eq("CODE", code).query()
                    );
                } else {
                    eList.addAll(getExamDao().queryForAll());
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(getClass().getName(),"Quering exams from local db",e);
        }

        Collections.sort(eList, new Comparator<Exam>() {
            @Override
            public int compare(Exam o1, Exam o2) {
                return o1.getCourseName().compareTo(o2.getCourseName());
            }
        });
        if (eList.isEmpty()) {
            showSortHeader(false);
            recyclerView.setVisibility(View.GONE);
            elm.setVisibility(View.VISIBLE);
        } else {
            showSortHeader(true);
            recyclerView.setVisibility(View.VISIBLE);
            elm.setVisibility(View.GONE);
        }
        this.adapter = new ExamRowRecyclerViewAdapter();
        adapter.setExams(eList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        recyclerView.setAdapter(adapter);
        super.setSortOptions(SORT_OPTIONS, this);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.parent = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        parent = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Comparator<Exam> comparator = null;
        switch (SORT_OPTIONS[position].toLowerCase()) {
            default:
            case "nome":
                comparator = new Comparator<Exam>() {
                    @Override
                    public int compare(Exam c1, Exam c2) {
                        return c1.getCourseName().compareTo(c2.getCourseName());
                    }
                };
                break;
            case "data":
                comparator = new Comparator<Exam>() {
                    @Override
                    public int compare(Exam o1, Exam o2) {
                        return o1.getDate().compareTo(o2.getDate());
                    }
                };
                break;
        }
        List<Exam> es = getExams();
        if (comparator != null & !es.isEmpty()) {
            Collections.sort(es, comparator);
            adapter.setExams(es);
        }
    }

    private List<Exam> getExams() {
        List<Exam> out = new ArrayList<>();
        try {
            out = getExamDao().queryForAll();
        } catch (SQLException e) {
            Log.e(TAG, e.getMessage());
            e.printStackTrace();
        }
        return out;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class ExamRowRecyclerViewAdapter extends RecyclerView.Adapter<ExamRowRecyclerViewAdapter.Holder> {
        class Holder extends RecyclerView.ViewHolder {
            TextView date, name, description;
            ImageView icon;

            public Holder(View view) {
                super(view);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        long id = getItem(getAdapterPosition()).getId();
                        DetailsActivity.start(parent, "EXAM", id);
                    }
                });
                date = (TextView) view.findViewById(R.id.date_value);
                name = (TextView) view.findViewById(R.id.name_of_course);
                description = (TextView) view.findViewById(R.id.decription_value);
                icon = (ImageView) view.findViewById(R.id.exam_status_icon);
            }

        }


        private List<Exam> exams;


        public void setExams(List<Exam> exams) {
            this.exams = exams;
            notifyDataSetChanged();
        }

        @Override
        public Holder onCreateViewHolder(final ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_exam, parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder viewHolder, int position) {
            Exam exam = getItem(position);
            if (exam.getBooked()) {
                viewHolder.icon.setImageResource(R.drawable.booked_exam);
            } else if (exam.getOpenBookingDate().after(new Date())) {
                viewHolder.icon.setImageResource(R.drawable.not_bookable_exam);
            } else {
                viewHolder.icon.setImageResource(R.drawable.bookable_exam);
            }
            viewHolder.description.setText("...");
            if (exam.getDescription() != null) {
                if (!exam.getDescription().equals(exam.getCourseName())) {
                    viewHolder.description.setText(exam.getDescription());
                }
            }

            viewHolder.name.setText(exam.getCourseName());
            viewHolder.date.setText((new SimpleDateFormat("dd/MM/yyyy")).format(exam.getDate()));
        }

        private Exam getItem(int position) {
            return exams.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).getId();
        }

        @Override
        public int getItemCount() {
            return exams.size();
        }
    }

}
