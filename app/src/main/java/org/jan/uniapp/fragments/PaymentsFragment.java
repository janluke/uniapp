package org.jan.uniapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jan.uniapp.R;
import org.jan.uniapp.domain.Payment;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentsFragment extends BaseListFragment {

    private RecyclerView recyclerView;
    private PaymentRowRecyclerViewAdapter adapter;

    public static PaymentsFragment newInstance() {
        return new PaymentsFragment();
    }

    public PaymentsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        showSortHeader(false);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.list_RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        adapter = new PaymentRowRecyclerViewAdapter();
        try {
            adapter.setPayments(getHelper().getPaymentsDao().queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.recyclerView.setAdapter(adapter);
        return view;
    }

    private class PaymentRowRecyclerViewAdapter extends RecyclerView.Adapter<PaymentRowRecyclerViewAdapter.Holder> {
        private List<Payment> payments;

        public void setPayments(List<Payment> payments) {
            this.payments = payments;
        }

        private Payment getItem(int position) {
            return payments.get(position);
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_payment, parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.ITALY);
            Payment p = getItem(position);
            holder.description.setText(p.getDescription());
            holder.dueDate.setText(sdf.format(p.getDueDate()));
            //solo per l interfaccia grafica
            String value = Double.toString(p.getValue()) + '€';
            if (value.length() < 8) {
                int l = value.length();
                for (int i = 0; i < (8 - l); i++) {
                    value += ' ';
                }
            }
            //
            holder.value.setText(value);
            int color = getResources().getColor(R.color.good);
            switch (p.getStatus()) {
                case NOT_CONFIRMED://orange
                    getResources().getColor(R.color.not_bad);
                    break;
                case NOT_PAID://red
                    getResources().getColor(R.color.bad);
                    break;
            }
            holder.value.setTextColor(color);
        }

        @Override
        public int getItemCount() {
            return payments.size();
        }

        public class Holder extends RecyclerView.ViewHolder {
            private TextView dueDate, value, description;

            public Holder(View itemView) {
                super(itemView);
                dueDate = (TextView) itemView.findViewById(R.id.due_date_field);
                value = (TextView) itemView.findViewById(R.id.value_field);
                description = (TextView) itemView.findViewById(R.id.description_field);
            }
        }
    }

}
