package org.jan.uniapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.jan.uniapp.R;
import org.jan.uniapp.domain.Course;
import org.jan.uniapp.repository.OrmLiteBaseFragment;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Gianluca on 06/04/2017.
 */

public class AverangeChartFragment extends OrmLiteBaseFragment {
    private final static String TAG = AverangeChartFragment.class.getSimpleName();
    private List<Course> courses = null;

    public static AverangeChartFragment newIstance() {
        return new AverangeChartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        LayoutInflater mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mInflater.inflate(R.layout.averange_chart, null, true);
        updateCourses();
        render(view);
        return view;

    }

    private void render(View view) {
        final TextView cfuField = (TextView) view.findViewById(R.id.averange_chart_selected_course_cfu);
        final TextView nameField = (TextView) view.findViewById(R.id.averange_chart_selected_course_name);
        CombinedChart chart = (CombinedChart) view.findViewById(R.id.combined_chart);
        CombinedData combinedData = getChartData();
        chart.setData(combinedData);
        //formatting axis
        chart.getAxisRight().setEnabled(false);
        chart.getAxisLeft().setDrawGridLines(false);
        XAxis xAxis = chart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMaximum(combinedData.getXMax() + 1);
        xAxis.setAxisMinimum(combinedData.getXMin() - 1);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                String label = "";
                if (value == Math.round(value)) {
                    try {
                        label = (new SimpleDateFormat("dd/MM/yy")).format(courses.get((int) value).getFinalResultDate());
                    } catch (Exception e) {
                    }
                }
                return label;
            }
        });
        xAxis.setLabelRotationAngle(30f);

        //hide description
        Description d = new Description();
        d.setEnabled(false);
        chart.setDescription(d);
        //and legend
        chart.getLegend().setEnabled(false);
        ///
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                Course selected = courses.get((int) e.getX());
                if (selected != null) {
                    cfuField.setVisibility(View.VISIBLE);
                    cfuField.setText(String.valueOf(selected.getCfu()) + " CFU");
                    nameField.setVisibility(View.VISIBLE);
                    nameField.setText(selected.getName());
                }
            }

            @Override
            public void onNothingSelected() {
                cfuField.setVisibility(View.INVISIBLE);
                nameField.setVisibility(View.INVISIBLE);
            }
        });
        chart.setDrawGridBackground(false);
        chart.invalidate();

    }

    public CombinedData getChartData() {
        Iterator<Course> it = courses.iterator();
        while (it.hasNext()) {
            if (it.next().getFinalResult() <= 1) {
                it.remove();
            }
        }
        List<BarEntry> coursBarValues = new ArrayList<>();
        List<Entry> averangeLineEntry = new ArrayList<>();
        Collections.sort(courses, new Comparator<Course>() {
            @Override
            public int compare(Course o1, Course o2) {
                return o1.getFinalResultDate().compareTo(o2.getFinalResultDate());
            }
        });
        float voteSum = 0, cfuSum = 0;
        for (Course c : courses) {
            coursBarValues.add(new BarEntry(courses.indexOf(c), c.getFinalResult()));
            voteSum += c.getCfu() * c.getFinalResult();
            cfuSum += c.getCfu();
            float averangeValue = voteSum / cfuSum;
            averangeLineEntry.add(new Entry(courses.indexOf(c), averangeValue));
        }

        BarDataSet barDataSet = new BarDataSet(coursBarValues, "");
        barDataSet.setColor(ColorTemplate.rgb("dab737"));
        barDataSet.setValueTextColor(ColorTemplate.rgb("b99b2e"));

        barDataSet.setValueTextSize(10f);

        BarData barData = new BarData(barDataSet);
        barData.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return String.valueOf(((int) value));
            }
        });


        LineDataSet lineDataSet = new LineDataSet(averangeLineEntry, "");
        lineDataSet.setColor(getResources().getColor(R.color.colorAccent));
        lineDataSet.setValueTextColor(getResources().getColor(R.color.colorPrimary));
        lineDataSet.setCircleColor(getResources().getColor(R.color.colorAccent));

        LineData lineData = new LineData(lineDataSet);
        lineData.setHighlightEnabled(false);
        lineData.setDrawValues(false);

        CombinedData combinedData = new CombinedData();
        combinedData.setData(barData);
        combinedData.setData(lineData);
        return combinedData;
    }

    public void updateCourses() {
        try {
            this.courses = getHelper().getCourseDao().queryForAll();
        } catch (SQLException e) {
            Log.e(TAG, "Quering courses", e);
            e.printStackTrace();
        }
    }
}
