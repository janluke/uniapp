package org.jan.uniapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.jan.uniapp.R;
import org.jan.uniapp.logic.LogicFacade;
import org.jan.uniapp.logic.UpdateTask;
import org.jan.uniapp.repository.OrmLiteBaseFragment;

import java.util.Arrays;

/**
 * Created by jan on 30/03/17.
 */

public class BaseListFragment extends OrmLiteBaseFragment {

    private SwipeRefreshLayout srl;
    private Spinner sortSpinner;
    private View sortHeader;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_layout, container, false);
        this.srl = (SwipeRefreshLayout) view.findViewById(R.id.fragment_list);
        this.sortSpinner = (Spinner) view.findViewById(R.id.list_sort_spinner);
        this.sortHeader = view.findViewById(R.id.list_sort_header);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srl.setRefreshing(true);
                //TODO update should be implemented by a service
                LogicFacade.Update(getActivity(), (UpdateTask.DbUpdaterListener) getActivity());

            }
        });

        return view;
    }

    protected void setSwipeRefreshEnabled(boolean enabled) {
        this.srl.setEnabled(false);
    }
    @Override
    public void onPause() {
        super.onPause();
        if (srl != null) {
            srl.setRefreshing(false);
            srl.destroyDrawingCache();
            srl.clearAnimation();
        }
    }

    public void showSortHeader(boolean show) {
        this.sortHeader.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    protected void setSortOptions(String[] options, AdapterView.OnItemSelectedListener listener) {

        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item);
        adapter.addAll(Arrays.asList(options));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setPrompt("Ordina per...");
        sortSpinner.setAdapter(adapter);
        sortSpinner.setOnItemSelectedListener(listener);
    }



}
