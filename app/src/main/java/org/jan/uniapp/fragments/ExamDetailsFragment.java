package org.jan.uniapp.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.jan.uniapp.R;
import org.jan.uniapp.domain.Exam;
import org.jan.uniapp.logic.AcceptAsyncTask;
import org.jan.uniapp.logic.JoinAsyncTask;
import org.jan.uniapp.logic.LogicFacade;
import org.jan.uniapp.repository.OrmLiteBaseFragment;
import org.jan.uniapp.views.DateCardView;

import java.sql.SQLException;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExamDetailsFragment extends OrmLiteBaseFragment
        implements JoinAsyncTask.JoinTaskListener, AcceptAsyncTask.AcceptTaskListener {

    private static final String TAG = ExamDetailsFragment.class.getSimpleName();
    private Exam exam;
    private View mProgressView;
    private Button joinButton;


    public ExamDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.exam_details, container, false);
        long id = getArguments().getLong("ID");
        if (id != -1) {
            try {
                this.exam = getHelper().getExamDao().queryForId(id);
            } catch (SQLException e) {
                e.printStackTrace();
                Log.e(TAG, "getting exam by id", e);
            }
            render(view);
        }

        return view;
    }

    private void render(View view) {
        //text
        TextView name = (TextView) view.findViewById(R.id.exam_details_name);
        TextView teacher = (TextView) view.findViewById(R.id.exam_details_teacher);
        TextView location = (TextView) view.findViewById(R.id.exam_details_location);
        TextView description = (TextView) view.findViewById(R.id.exam_details_description);
        TextView subscribed_count = (TextView) view.findViewById(R.id.exam_details_subscribed_count);
        DateCardView dateView = (DateCardView) view.findViewById(R.id.date_view);
        CardView voteView = (CardView) view.findViewById(R.id.exam_details_vote_container);

        ///buttons
        Button acceptButton = (Button) view.findViewById(R.id.exam_details_accept_button);
        Button refuseButton = (Button) view.findViewById(R.id.exam_details_refuse_button);
        this.joinButton = (Button) view.findViewById(R.id.exam_details_join_button);

        //progress
        mProgressView = view.findViewById(R.id.exam_details_action_progress);

        Log.v(TAG, "Beging exam details rendering");
        //rendering
        name.setText(exam.getCourseName());
        description.setText(exam.getDescription());
        subscribed_count.setText(String.valueOf(exam.getBookedStudents()));
        if ((exam.getTeacher() == null) || (exam.getTeacher().isEmpty())) {
            teacher.setVisibility(View.GONE);
        } else {
            teacher.setText(exam.getTeacher());
        }
        if ((exam.getLocation() == null) || (exam.getLocation().isEmpty())) {
            location.setVisibility(View.GONE);
        } else {
            location.setText(exam.getLocation());
        }
        dateView.setDate(exam.getDate());



        voteView.setVisibility(View.GONE);

        if (exam.getBooked()) {
            joinButton.setText("Cancella iscrizione");
            if (exam.getResult() != null) {
                joinButton.setVisibility(View.GONE);
                acceptButton.setVisibility(View.VISIBLE);
                refuseButton.setVisibility(View.VISIBLE);
                voteView.setVisibility(View.VISIBLE);
            }
        } else if (exam.getOpenBookingDate().after(new Date())) {
            joinButton.setEnabled(false);
        }
        final ExamDetailsFragment edf = this;
        joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                if (exam.getBooked()) {
                    LogicFacade.unjoin(edf.getContext(), edf, exam);
                } else {
                    LogicFacade.join(edf.getContext(), edf, exam);
                }
            }
        });

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                LogicFacade.accept(edf.getContext(), edf, exam);
            }
        });

        refuseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                LogicFacade.refuse(edf.getContext(), edf, exam);
            }
        });


    }
    private static String getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return new DateFormatSymbols(Locale.ITALY).getMonths()[cal.get(Calendar.MONTH)];
    }



    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }



    @Override
    public void onJoinTaskComplete(String[] params, String erro) {

        showProgress(false);
        if (params[0].equals("JOIN")) {
            if (erro == null) {
                joinButton.setText("Cancella iscrizione");
            } else {
                Toast.makeText(getContext(), "Error:\n" + erro, Toast.LENGTH_SHORT).show();
            }
        } else {
            if (erro == null) {
                joinButton.setText("Iscriviti");
            } else {
                Toast.makeText(getContext(), "Error:\n" + erro, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onAcceptTaskComplete(String[] params, String erro) {
        showProgress(false);
        if (erro != null) {
            Toast.makeText(getContext(), "Error:\n" + erro, Toast.LENGTH_SHORT).show();
        } else {
            //TUTTO OKAY
        }

    }
}
