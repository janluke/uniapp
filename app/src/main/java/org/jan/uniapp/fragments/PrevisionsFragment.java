package org.jan.uniapp.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;

import org.jan.uniapp.R;
import org.jan.uniapp.domain.Course;
import org.jan.uniapp.repository.OrmLiteBaseFragment;
import org.jan.uniapp.utils.PrevisionUtil;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Gianluca on 11/04/2017.
 */

public class PrevisionsFragment extends OrmLiteBaseFragment {
    private static final String TAG = PrevisionsFragment.class.getSimpleName();

    private LayoutInflater inflater;
    private RecyclerView recyclerView;
    private TextView degree;
    private TextView averange;
    private PrevisionExamRowRecyclerAdapter adapter;
    private PrevisionUtil prevision;

    public static PrevisionsFragment newInstance() {
        return new PrevisionsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.inflater = inflater;
        View view = inflater.inflate(R.layout.fragment_prevision, container, false);
        this.averange = (TextView) view.findViewById(R.id.prevision_averange_vote);
        this.degree = (TextView) view.findViewById(R.id.prevision_degree_vote);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.list_RecyclerView);
        this.adapter = new PrevisionExamRowRecyclerAdapter(getNotVotedCourses());

        this.averange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAverangeSetterDialog();
            }
        });
        this.degree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDegreeVoteSetterDialog();
            }
        });
        view.findViewById(R.id.list_sort_header).setVisibility(View.GONE);//nascondiamo l'header
        view.findViewById(R.id.fragment_list).setEnabled(false);//disabilitiamo refreshlayout
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        recyclerView.setAdapter(adapter);

        Spinner spinner = (Spinner) view.findViewById(R.id.prevision_mode_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getContext(),
                android.R.layout.simple_spinner_item);
        adapter.addAll("Media", "Voti");
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                prevision.setMode(position);
                updateUi();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinner.setAdapter(adapter);
        prevision = new PrevisionUtil(getSignificatCourse());
        updateUi();
        return view;
    }

    private void showAverangeSetterDialog() {

        final AlertDialog.Builder d = new AlertDialog.Builder(getActivity());
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        d.setTitle("Media");
        d.setMessage("Scegli il tuo obbiettivo");
        d.setView(dialogView);
        final NumberPicker np = (NumberPicker) dialogView.findViewById(R.id.dialog_number_picker);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np.setMaxValue((int) Math.round(prevision.getMaxAverange()));
        np.setMinValue((int) Math.round(prevision.getMinAverange()));
        d.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                prevision.setAverange(np.getValue());
                updateUi();
            }
        });
        d.create().show();
    }
    private void showDegreeVoteSetterDialog() {
        final AlertDialog.Builder d = new AlertDialog.Builder(getActivity());
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        d.setTitle("Base di laurea");
        d.setMessage("Scegli il tuo obbiettivo");
        d.setView(dialogView);
        final NumberPicker np = (NumberPicker) dialogView.findViewById(R.id.dialog_number_picker);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np.setMaxValue((int) Math.round(prevision.getMaxDegreeVote()));
        np.setMinValue((int) Math.round(prevision.getMinDegreeVote()));
        d.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                prevision.setDegreeVote(np.getValue());
                updateUi();
            }
        });
        d.create().show();
    }

    private void showVoteSetterDialog(final Course course) {

        final AlertDialog.Builder d = new AlertDialog.Builder(getActivity());
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        d.setTitle(course.getName());
        d.setMessage("Scegli il tuo obbiettivo");
        d.setView(dialogView);
        final NumberPicker np = (NumberPicker) dialogView.findViewById(R.id.dialog_number_picker);
        np.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np.setMaxValue(31);
        np.setMinValue(18);
        d.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                prevision.setVote(course, np.getValue());
                updateUi();
            }
        });
        d.create().show();
    }


    private void updateUi() {
        for (Course c : getNotVotedCourses()) {
            Integer result = prevision.getVote(c);
            if (result != null && result > 0) {
                adapter.putResult(c, result);
            }
        }
        String avStr = String.valueOf(prevision.getAverange()),
                deStr = String.valueOf(prevision.getDegreeVote());
        this.averange.setText(avStr
                .substring(0, avStr.length() > 5 ? 5 : avStr.length()));
        this.degree.setText(deStr
                .substring(0, deStr.length() > 5 ? 5 : deStr.length()));
    }


    private List<Course> getNotVotedCourses() {
        List<Course> courses = getSignificatCourse();
        Iterator<Course> it = courses.iterator();
        while (it.hasNext()) {
            Course c = it.next();
            if (c.getFinalResult() > 0) {
                it.remove();
            }
        }

        return courses;
    }

    private List<Course> getSignificatCourse() {
        List<Course> courses = null;
        try {
            courses = getHelper().getCourseDao().queryForAll();
            Iterator<Course> it = courses.iterator();
            while (it.hasNext()) {
                Course c = it.next();
                if (c.getCfu() == 0) {
                    it.remove();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return courses;
    }

    //
    private class PrevisionExamRowRecyclerAdapter extends
            RecyclerView.Adapter<PrevisionExamRowRecyclerAdapter.Holder> {
        private List<Course> courses;
        private HashMap<String, Integer> results;//course code -> result

        public PrevisionExamRowRecyclerAdapter(List<Course> courses) {
            this.courses = courses;
            results = new HashMap<>();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_course, parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder h, int position) {
            Course c = courses.get(position);
            h.name.setText(c.getName());
            h.cfu.setText(String.valueOf(c.getCfu()));
            int result_value = 0;
            if (results.containsKey(c.getCode()))
                result_value = results.get(c.getCode());
            renderResult(h.voteImage, result_value);
        }

        @Override
        public int getItemCount() {
            return courses.size();
        }

        public void renderResult(ImageView img, int result) {
            int color = getResources().getColor(R.color.colorAccent);
            String text = new String();
            if (result > 1) {
                text = String.valueOf(result);
                if (result > 25) {
                    color = getResources().getColor(R.color.good);
                    if (result > 30) text = "30L";
                } else if (result > 20) {
                    color = getResources().getColor(R.color.not_bad);
                } else {
                    color = getResources().getColor(R.color.bad);
                }
            } else if (result == 1) {
                text = "✓";
                color = getResources().getColor(R.color.good);
            }
            TextDrawable drawable1 = TextDrawable.builder()
                    .buildRound(text, color);
            img.setImageDrawable(drawable1);
        }

        class Holder extends RecyclerView.ViewHolder {
            TextView cfu, name;
            ImageView voteImage;

            public Holder(final View view) {
                super(view);
                cfu = (TextView) view.findViewById(R.id.cfu_value);
                name = (TextView) view.findViewById(R.id.name_of_course);
                voteImage = (ImageView) view.findViewById(R.id.final_result_image);
                ((TextView) view.findViewById(R.id.cfu_value_label)).setText("CFU");
                view.findViewById(R.id.year_value).setVisibility(View.INVISIBLE);
                view.findViewById(R.id.year_value_label).setVisibility(View.INVISIBLE);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showVoteSetterDialog(courses.get(getAdapterPosition()));
                    }
                });
                view.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        final String name = courses.get(getAdapterPosition()).getName();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(name);
                        builder.setMessage(
                                "Vuoi nascondere questo esame nelle previsioni?"
                        );
                        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                prevision.remove(courses.get(getAdapterPosition()));
                                courses.remove(getAdapterPosition());
                                Snackbar.make(view, name + " nascosto", Snackbar.LENGTH_LONG).show();
                                notifyItemRemoved(getAdapterPosition());
                                notifyDataSetChanged();
                            }
                        });
                        builder.setNegativeButton("No", null);
                        builder.create().show();
                        return false;
                    }
                });
            }

        }

        public void putResult(Course course, Integer value) {
            results.put(course.getCode(), value);
            notifyDataSetChanged();
        }
    }
}
