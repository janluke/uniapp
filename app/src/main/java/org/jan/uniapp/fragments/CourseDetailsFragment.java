package org.jan.uniapp.fragments;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.jan.uniapp.R;
import org.jan.uniapp.domain.Course;
import org.jan.uniapp.repository.OrmLiteBaseFragment;
import org.jan.uniapp.views.DateCardView;

import java.sql.SQLException;

/**
 * A placeholder fragment containing a simple view.
 */
public class CourseDetailsFragment extends OrmLiteBaseFragment {
    private Course course;

    public CourseDetailsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.course_details, container, false);
        long id = getArguments().getLong("ID");
        if (id != -1) {
            try {
                this.course = getHelper().getCourseDao().queryForId(id);
            } catch (SQLException e) {
                e.printStackTrace();
                Log.e(getClass().getName(), "Quering course", e);
            }
            render(view);
        }

        return view;
    }

    private void render(View view) {
        TextView name = (TextView) view.findViewById(R.id.course_details_name);
        TextView cfu = (TextView) view.findViewById(R.id.course_details_cfu_value);
        Button examsButton = (Button) view.findViewById(R.id.course_details_show_exam_button);
        if (course.getFinalResult() != 0) {
            examsButton.setVisibility(View.GONE);
            DateCardView date = (DateCardView) view.findViewById(R.id.course_details_date_view);
            TextView vote = (TextView) view.findViewById(R.id.course_details_vote_value);
            date.setDate(course.getFinalResultDate());
            int fr = course.getFinalResult();
            vote.setText(fr != 1 ? String.valueOf(fr) : "I");
        } else {
            CardView cv = (CardView) view.findViewById(R.id.course_details_vote_container);
            cv.setVisibility(View.GONE);
            cv = (CardView) view.findViewById(R.id.course_details_date_view);
            cv.setVisibility(View.GONE);

        }
        cfu.setText(String.valueOf(course.getCfu()));
        name.setText(course.getName());
        //TODO: il resto
    }


}
